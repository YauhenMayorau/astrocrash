from livewires import games, color
import math, random

games.init(screen_width = 1440, screen_height = 980, fps = 50)

class Wrapper(games.Sprite):
    """ Sprite that move around window."""
    def update(self):
        if self.top > games.screen.height:
            self.bottom = 0
        if self.bottom < 0:
            self.top = games.screen.height
        if self.left > games.screen.width:
            self.right = 0
        if self.right < 0:
            self.left = games.screen.width
    def die(self):
        """Destroys the object."""
        self.destroy()

class Collider(Wrapper):
    """ Collider. Objecct that collide and destroy."""
    def update(self):
        """Checks. Whether there are no sprites. visually overlapping with the given. """
        super(Collider, self).update()
        if self.overlapping_sprites:
            for sprite in self.overlapping_sprites:
                sprite.die()
            self.die()
    def die(self):
        """Destroy object with explosion """
        new_explosion = Explosion(x = self.x, y = self.y)
        games.screen.add(new_explosion)
        self.destroy()

class Asteroid(Wrapper):
    """ Asteroid moves rectilinearly across the screen. """
    SMALL = 1
    MEDIUM = 2
    LARGE = 3
    SPAWN = 2
    POINTS = 30
    total = 0
    images = {SMALL : games.load_image("images/asteroid_small.png"), MEDIUM : games.load_image("images/asteroid_medium.png"),
              LARGE: games. load_image("images/asteroid_big.png")}
    SPEED = 2
    def __init__(self, game, x, y, size):
        """ Initializes a sprite with an asteroid image. """
        Asteroid.total += 1
        self.game = game
        super(Asteroid, self).__init__(
            image = Asteroid.images[size], x = x, y = y,
            dx = random.choice([1, - 1]) * Asteroid.SPEED * random.random()/size,
            dy = random.choice([1, - 1]) * Asteroid.SPEED * random.random()/size)
        self.size = size

    def update(self):
        """ Moving asteroid around the screen."""
        if self.top > games.screen.height:
            self.bottom = 0
        if self .bottom < 0:
            self.top = games.screen.height
        if self.left > games.screen.width:
            self.right = 0
        if self.right < 0:
            self.left = games.screen.width

    def die(self):
        """ destroy asteroid. """
        # If the size of the asteroid is large or medium. replace it with two smaller asteroids
        if self.size != Asteroid.SMALL:
            for i in range(Asteroid.SPAWN):
                new_asteroid = Asteroid(game = self.game,
                                        x=self.x,
                                        y= self.y,
                                        size = self.size - 1)
                games.screen.add(new_asteroid)
        super(Asteroid, self).die()
        Asteroid.total -= 1
        self.game.score.value += int(Asteroid.POINTS / self.size)
        self.game.score.right = games.screen.width - 10
        # if there are no more asteroids left. go to the next level
        if Asteroid.total == 0:
            self.game.advance()

class Ship(Collider):
    """ Rotating spaceship. """
    image = games.load_image("images/ship.png")
    sound = games.load_sound("sound/ship.wav")
    ROTATION_STEP = 3
    VELOCITY_STEP = 0.1
    MISSILE_DELAY = 50
    VELOCITY_МАХ = 3

    def __init__(self, game, x, y):
        """ Initializes the sprite with the image of the spaceship."""
        self.game = game
        super(Ship, self).__init__(image=Ship.image,
                                   x = x,
                                   y = y)
        self.missile_wait = 0
    def update(self):
        """ Moves the ship in a certain way. based on the keys. """
        if games.keyboard.is_pressed(games.K_w):
            self.y -= 1
        if games.keyboard.is_pressed(games.K_s):
            self.y += 1
        if games.keyboard.is_pressed(games.K_a):
            self.x -= 1
        if games.keyboard.is_pressed(games.K_d):
            self.x += 1
        if games.keyboard.is_pressed(games.K_RIGHT):
            self.angle += Ship.ROTATION_STEP
        if games.keyboard.is_pressed(games.K_LEFT):
            self.angle -= Ship.ROTATION_STEP
        if games.keyboard.is_pressed(games.K_1):
            self.angle = 0
        if games.keyboard.is_pressed(games.K_2):
            self.angle = 90
        if games.keyboard.is_pressed(games.K_3):
            self.angle = 180
        if games.keyboard.is_pressed(games.K_4):
            self.angle = 270
        # the ship makes a dash
        if games.keyboard.is_pressed(games.K_UP):
            # Ship.sound.play()
        # change in the ship's horizontal and vertical speed, taking into account the angle of rotation
            angle = self.angle * math.pi / 180  # conversion to radians
            self.dx += Ship.VELOCITY_STEP * math.sin(angle)
            self.dy += Ship.VELOCITY_STEP * -math.cos(angle)
        if games.keyboard.is_pressed(games.K_DOWN):
            # Ship.sound.play()
            angle = self.angle * math.pi / 180  # conversion to radians
            self.dx = min(max(self.dx, -Ship.VELOCITY_МАХ), Ship.VELOCITY_МАХ)
            self.dy = min(max(self.dy, -Ship.VELOCITY_МАХ), Ship.VELOCITY_МАХ)
        # the ship will "bend" the screen
        if self.top > games.screen.height:
            self.bottom = 0
        if self.bottom < 0:
            self.top = games.screen.height
        if self.left > games.screen.width:
            self.right = 0
        if self.right < 0:
            self.left = games.screen.width
        # If the launch of the next rocket is not yet authorized. subtract 1 from the length of the remaining waiting interval
        if self.missile_wait > 0:
            self.missile_wait -= 1
        # if the Spacebar is pressed and the interval has expired. to launch a rocket
        if games.keyboard.is_pressed(games.K_SPACE) and self.missile_wait == 0:
            new_missile = Missile(self.x, self.y, self.angle)
            games.screen.add(new_missile)
            self.missile_wait = Ship.MISSILE_DELAY
        super(Ship, self).update()

    def die(self):
        """Destroys the ship and completes the game. """
        self.game.end()
        super(Ship, self).die()


class Missile(Collider):
    """ Rocket. which player spacecraft can release."""
    image = games.load_image("images/missile.png")
    sound = games.load_sound("sound/missile.wav")
    BUFFER = 120
    VELOCITY_FACTOR = 7
    LIFETIME = 40
    def __init__(self, ship_x, ship_y, ship_angle):
        """ Initializes a sprite with a rocket image."""
        Missile.sound.play()
        # conversion to radians
        angle = ship_angle * math.pi / 180
        # calculation of the initial position of the rocket
        buffer_x = Missile.BUFFER * math.sin(angle)
        buffer_y = Missile.BUFFER * -math.cos(angle)
        x = ship_x + buffer_x
        y = ship_y + buffer_y
        # calculation of the horizontal and vertical velocity of the rocket
        dx = Missile.VELOCITY_FACTOR * math.sin(angle)
        dy = Missile.VELOCITY_FACTOR * -math.cos(angle)
        # rocket creation
        super(Missile,self).__init__(image = Missile.image,
                                     x = x,
                                     y = y,
                                     dx = dx,
                                     dy = dy)
        self.lifetime = Missile.LIFETIME

    def update(self):
        """ Moves the rocket."""
        # If the "expiration date" of the rocket expired. it is destroyed
        self.lifetime -= 1
        if self. lifetime == 0:
            self.destroy()
        # the rocket will "bend" the screen
        if self.top > games.screen.height:
            self.bottom = 0
        if self.bottom < 0:
            self.top = games.screen.height
        if self.left > games.screen.width:
            self.right = 0
        if self.right < 0:
            self.left = games.screen.width
        super(Missile, self).update()


class Explosion(games.Animation):
    """Animated explosion"""
    sound = games.load_sound("sound/explosion.wav")
    images = ["images/explosion0.png",
                       "images/explosion1.png",
                       "images/explosion2.png",
                       "images/explosion3.png",
                       "images/explosion4.png",
                       "images/explosion5.png",
                       "images/explosion6.png",
                       "images/explosion7.png",
                       "images/explosion8.png",
                       "images/explosion9.png",
                       "images/explosion10.png",
                       "images/explosion11.png",
                       "images/explosion12.png",
                       "images/explosion13.png",
                       "images/explosion14.png"]

    def __init__(self, x, y):
        super(Explosion, self).__init__(images = Explosion.images,
                                        x = x, y = y,
                                        repeat_interval = 4,
                                        n_repeats = 1,
                                        is_collideable = False)
        Explosion.sound.play()

class Game(object) :
    """ The game. """

    def __init__(self):
        """ Initializes a Game object."""
        # selection of the initial game level
        self.level = 0
        #load sound. accompanying the transition to the next level
        # self.sound = games.load_sound("sound/level.wav")
        #object creation. in which the current account will be kept
        self.score = games.Text(value =0,
                                size = 30,
                                color = color.white,
                                top = 5,
                                right = games.screen.width - 10,
                                is_collideable = False)
        games.screen.add(self.score)
        #creation of a ship. which will be controlled by the player
        self.ship = Ship(game = self,
                         x = games.screen.width/2,
                         y = games.screen.height/2)
        games.screen.add(self.ship)

    def play(self):
        """ Starts the game. """
        # launch of a musical theme
        games.music.load("sound/theme.mp3")
        games.music.play(-1)
        # load and assign a background picture
        nebula_image = games.load_image("images/nebula.jpg", transparent=False)
        games.screen.background = nebula_image
        # transition to level 1 -
        self.advance()
        # beginning of the game
        games.screen.mainloop()

    def advance(self):
        """Moves the game to the next level."""
        self.level += 1
        # reserved space around the ship
        BUFFER = 150
        # creation of new asteroids
        for i in range(self.level):
            # calculate x and y, so that from the ship they are at least BUFFER pixels
            # first, we select the minimum indents in the horizontal and vertical
            x_min = random.randrange(BUFFER)
            y_min = BUFFER - x_min
            # proceeding from these minima, we will generate distances from the ship horizontally and vertically
            x_distance = random.randrange(x_min, games.screen.width - x_min)
            y_distance = random.randrange(y_min, games.screen.height - y_min)
            # proceeding from these distances. calculate the screen coordinates
            x = self.ship.x + x_distance
            y = self.ship.y + y_distance
            # if it is needed. return the object inside the window
            x %= games.screen.width
            y %= games.screen.height
            # reate an asteroid
            new_asteroid = Asteroid(game = self,
                                    x = x,
                                    y = y,
                                    size= Asteroid.LARGE)
            games.screen.add(new_asteroid)

        # displaying the level number
        level_message = games.Message(value="Level" + str(self.level),
                                      size = 40,
                                      color = color.yellow,
                                      x = games.screen.width / 2,
                                      y = games.screen.width / 10,
                                      lifetime = 3 * games.screen.fps,
                                      is_collideable = False)
        games.screen.add(level_message)
        # sound transition effect (except for the 1st level)
        # if self.leve > 1:
            # self.sound.play()

    def end(self):
        """ Finishes the game. """
        # 5-second display of 'Game Over'
        end_message = games.Message(value = "Game Over",
                                    size = 90,
                                    color = color.red,
                                    x = games.screen.width/2,
                                    y = games.screen.height/2,
                                    lifetime = 5 * games.screen.fps,
                                    after_death = games.screen.quit,
                                    is_collideable = False)
        games.screen.add(end_message)

def main():
    astrocrash = Game()
    astrocrash.play()

# go!
if __name__ == '__main__':
    main()